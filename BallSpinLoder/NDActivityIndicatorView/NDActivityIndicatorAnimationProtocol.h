//
//  NDActivityIndicatorAnimationProtocol.h
//  BallSpinLoder
//
//  Created by Apple on 28/08/16.
//  Copyright © 2016 NikhilDevgire. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol NDActivityIndicatorAnimationProtocol <NSObject>

- (void)setupAnimationInLayer:(CALayer *)layer withSize:(CGSize)size tintColor:(UIColor *)tintColor;

@end
