//
//  NDActivityIndicatorView.h
//  BallSpinLoder
//
//  Created by Apple on 28/08/16.
//  Copyright © 2016 NikhilDevgire. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, NDActivityIndicatorAnimationType) {
    NDActivityIndicatorAnimationTypeBallSpinFadeLoader
};


@interface NDActivityIndicatorView : UIView

- (id)initWithType:(NDActivityIndicatorAnimationType)type;
- (id)initWithType:(NDActivityIndicatorAnimationType)type tintColor:(UIColor *)tintColor;
- (id)initWithType:(NDActivityIndicatorAnimationType)type tintColor:(UIColor *)tintColor size:(CGFloat)size;

@property (nonatomic) NDActivityIndicatorAnimationType type;
@property (nonatomic, strong) UIColor *tintColor;
@property (nonatomic) CGFloat size;

@property (nonatomic, readonly) BOOL animating;

- (void)startAnimating;
- (void)stopAnimating;

@end
