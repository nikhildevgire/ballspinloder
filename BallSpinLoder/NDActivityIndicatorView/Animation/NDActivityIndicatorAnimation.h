//
//  NDActivityIndicatorAnimation.h
//  BallSpinLoder
//
//  Created by Apple on 28/08/16.
//  Copyright © 2016 NikhilDevgire. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NDActivityIndicatorAnimationProtocol.h"

@interface NDActivityIndicatorAnimation : NSObject <NDActivityIndicatorAnimationProtocol>

- (CABasicAnimation *)createBasicAnimationWithKeyPath:(NSString *)keyPath;
- (CAKeyframeAnimation *)createKeyframeAnimationWithKeyPath:(NSString *)keyPath;
- (CAAnimationGroup *)createAnimationGroup;

@end
