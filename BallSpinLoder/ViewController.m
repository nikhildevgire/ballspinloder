//
//  ViewController.m
//  BallSpinLoder
//
//  Created by Apple on 28/08/16.
//  Copyright © 2016 NikhilDevgire. All rights reserved.
//

#import "ViewController.h"
#import "NDActivityIndicatorView.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    
    self.view.backgroundColor = [UIColor colorWithRed:227/255.0f green:85/255.0f blue:101/255.0f alpha:1.0f];

    
    NDActivityIndicatorView *activityIndicatorView = [[NDActivityIndicatorView alloc] initWithType:NDActivityIndicatorAnimationTypeBallSpinFadeLoader tintColor:[UIColor whiteColor]];
    CGFloat width = self.view.bounds.size.width;
    CGFloat height = self.view.bounds.size.height;
    
    activityIndicatorView.frame = CGRectMake(0, 0, width, height);
    [self.view addSubview:activityIndicatorView];
    [activityIndicatorView startAnimating];
    
    
    
    
    
//    NSArray *activityTypes = @[@(NDActivityIndicatorAnimationTypeBallSpinFadeLoader)];
//    
//    for (int i = 0; i < activityTypes.count; i++) {
//        NDActivityIndicatorView *activityIndicatorView = [[NDActivityIndicatorView alloc] initWithType:(NDActivityIndicatorAnimationType)[activityTypes[i] integerValue] tintColor:[UIColor whiteColor]];
//        CGFloat width = self.view.bounds.size.width / 5.0f;
//        CGFloat height = self.view.bounds.size.height / 7.0f;
//        
//        activityIndicatorView.frame = CGRectMake(width * (i % 7), height * (int)(i / 7), width, height);
//        [self.view addSubview:activityIndicatorView];
//        [activityIndicatorView startAnimating];
//    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
