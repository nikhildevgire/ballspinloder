//
//  AppDelegate.h
//  BallSpinLoder
//
//  Created by Apple on 28/08/16.
//  Copyright © 2016 NikhilDevgire. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

